import * as E from 'fp-ts/lib/Either'
import { generateReqBody, getBomRefFor, npmSeverityToBomSeverity, npmVectorStringToBomMethod, toBomVulns } from '../../src/lib/npm'
import type { BomFile } from '../../src/types'

describe('lib/npm', () => {
  describe('generateReqBody', () => {
    it('lefts on no components', () => {
      const bom: BomFile = {
        bomFormat: 'CycloneDX',
        specVersion: '1.4',
        version: 1,
      }
      expect(generateReqBody(bom)).toBeNone()
    })
    it('rights on components', () => {
      const bom: BomFile = {
        bomFormat: 'CycloneDX',
        specVersion: '1.4',
        version: 1,
        components: [
          { type: 'library', name: 'comp1', version: '1.0' },
          { type: 'library', name: 'comp1', version: '1.0' },
          { type: 'library', name: 'comp1', version: '1.1' },
          { type: 'library', group: '@group', name: 'comp2', version: '0.1' },
          { type: 'library', name: 'comp3' },
        ],
      }
      expect(generateReqBody(bom)).toEqualSome({
        comp1: [ '1.0', '1.1' ],
        '@group/comp2': [ '0.1' ],
        comp3: [],
      })
    })
  })

  describe('getBomRefFor', () => {
    it('works', () => {
      const components = [
        {
          type: <const>'library',
          name: 'name',
          purl: 'pkg:npm/%40group/name@1.1',
          version: '1.1',
          'bom-ref': 'my-ref',
        },
      ]
      expect(getBomRefFor('@group/name', '<1.2')(components)).toEqual([ {
        ref: 'my-ref',
        versions: [ { range: 'vers:npm/<1.2' } ],
      } ])
    })
    it('matches on name', () => {
      const components = [
        {
          type: <const>'library',
          name: 'name',
          purl: 'pkg:npm/name@1.1',
          version: '1.1',
          'bom-ref': 'my-ref',
        },
      ]
      expect(getBomRefFor('other-name', '<1.2')(components)).toEqual([])
    })
    it('matches on version', () => {
      const components = [
        {
          type: <const>'library',
          name: 'name',
          purl: 'pkg:npm/name@1.1',
          version: '1.1',
          'bom-ref': 'my-ref',
        },
      ]
      expect(getBomRefFor('name', '<1.0')(components)).toEqual([])
    })
    it('handles npm version ranges', () => {
      const components = [
        {
          type: <const>'library',
          name: 'name',
          purl: 'pkg:npm/%40group/name@1.1',
          version: '1.1',
          'bom-ref': 'my-ref',
        },
      ]
      expect(getBomRefFor('@group/name', '>=1.0 <1.2')(components)).toEqual([ {
        ref: 'my-ref',
        versions: [ { range: 'vers:npm/>=1.0|<1.2' } ],
      } ])
    })
    it('handles npm version ranges 2', () => {
      const components = [
        {
          type: <const>'library',
          name: 'name',
          purl: 'pkg:npm/%40group/name@1.1',
          version: '1.1',
          'bom-ref': 'my-ref',
        },
      ]
      expect(getBomRefFor('@group/name', '>=0.5 <1.0')(components)).toEqual([])
    })
  })

  describe('npmVectorStringToBomMethod', () => {
    it('returns other on left', () => {
      expect(npmVectorStringToBomMethod(E.left('something'))).toBe('other')
    })
    it('returns other on unknown', () => {
      expect(npmVectorStringToBomMethod(E.right('something'))).toBe('other')
    })
    it('handles CVSSv31', () => {
      expect(npmVectorStringToBomMethod(E.right('CVSS:3.1'))).toBe('CVSSv31')
    })
    it('handles CVSSv3', () => {
      expect(npmVectorStringToBomMethod(E.right('CVSS:3'))).toBe('CVSSv3')
    })
    it('handles CVSSv2', () => {
      expect(npmVectorStringToBomMethod(E.right('CVSS:2'))).toBe('CVSSv2')
    })
    it('handles OWASP', () => {
      expect(npmVectorStringToBomMethod(E.right('OWASP'))).toBe('OWASP')
    })
  })

  describe('npmSeverityToBomSeverity', () => {
    it('returns unknown on left', () => {
      expect(npmSeverityToBomSeverity('something')).toBe('unknown')
    })
    it('handles critical', () => {
      expect(npmSeverityToBomSeverity('critical')).toBe('critical')
    })
    it('handles high', () => {
      expect(npmSeverityToBomSeverity('high')).toBe('high')
    })
    it('handles moderate', () => {
      expect(npmSeverityToBomSeverity('moderate')).toBe('medium')
    })
    it('handles low', () => {
      expect(npmSeverityToBomSeverity('low')).toBe('low')
    })
  })

  describe('toBomVulns', () => {
    it('works', () => {
      const
        bom = {
          bomFormat: <const>'CycloneDX',
          specVersion: '1.3',
          version: 1,
          components: [ {
            'bom-ref': 'my-ref',
            name: 'name',
            type: <const>'library',
            version: '0.5',
            purl: 'pkg:npm/name@0.5',
          } ],
        },
        result = {
          name: [ {
            id: 101,
            url: 'https://example.com/',
            title: 'title',
            severity: 'high',
            vulnerable_versions: '<0.7',
            cwe: [ 'CWE-77' ],
            cvss: {
              score: 7.2,
              vectorString: 'CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H',
            },
          } ],
        }
      expect(toBomVulns(bom)(result)).toEqual([ {
        affects: [ {
          ref: 'my-ref',
          versions: [ { range: 'vers:npm/<0.7' } ],
        } ],
        cwes: [ 77 ],
        description: 'title',
        id: '101',
        advisories: [ { url: 'https://example.com/' } ],
        ratings: [ {
          method: 'CVSSv31',
          score: 7.2,
          severity: 'high',
          vector: 'AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H',
        } ],
      } ])
    })
  })
})
