import { addTool, incBomVersion, insertVulns, setBomVersionNumber, setTimestamp } from '../../src/lib/bom'
import { name, version } from '../../package.json'
import type { BomFile, BomVulnerabilities } from '../../src/types'

describe('lib/bom', () => {
  describe('insertVulns', () => {
    it('inserts vulnerabilities', () => {
      const
        bom: BomFile = {
          bomFormat: <const>'CycloneDX',
          specVersion: '1.3',
          version: 1,
          components: [ {
            'bom-ref': 'my-ref',
            name: 'name',
            type: <const>'library',
            version: '0.5',
            purl: 'pkg:npm/name@0.5',
          } ],
        },
        vulns: BomVulnerabilities = [ {
          affects: [ { ref: 'my-ref' } ],
          cwes: [ 77 ],
          description: 'title',
          id: '101',
          advisories: [ { url: 'https://example.com/' } ],
          ratings: [ {
            method: 'CVSSv31',
            score: 7.2,
            severity: 'high',
            vector: 'AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H',
          } ],
        } ]
      expect(insertVulns(bom)(vulns)).toEqual({
        bomFormat: <const>'CycloneDX',
        specVersion: '1.3',
        version: 1,
        components: [ {
          'bom-ref': 'my-ref',
          name: 'name',
          type: <const>'library',
          version: '0.5',
          purl: 'pkg:npm/name@0.5',
        } ],
        vulnerabilities: [ {
          affects: [ { ref: 'my-ref' } ],
          cwes: [ 77 ],
          description: 'title',
          id: '101',
          advisories: [ { url: 'https://example.com/' } ],
          ratings: [ {
            method: 'CVSSv31',
            score: 7.2,
            severity: 'high',
            vector: 'AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H',
          } ],
        } ],
      })
    })
  })

  describe('incBomVersion', () => {
    it('works', () => {
      const bom: BomFile = {
        bomFormat: <const>'CycloneDX',
        specVersion: '1.3',
        version: 2,
      }
      expect(incBomVersion(bom)).toEqual({
        bomFormat: 'CycloneDX',
        specVersion: '1.3',
        version: 3,
      })
    })
  })

  describe('setBomVersionNumber', () => {
    it('works', () => {
      const bom: BomFile = {
        bomFormat: <const>'CycloneDX',
        specVersion: '1.3',
        version: 1,
      }
      expect(setBomVersionNumber(bom)).toEqual({
        bomFormat: 'CycloneDX',
        specVersion: '1.4',
        version: 1,
      })
    })
  })

  describe('setTimestamp', () => {
    it('works', () => {
      const
        bom: BomFile = {
          bomFormat: <const>'CycloneDX',
          specVersion: '1.3',
          version: 1,
        },
        now = new Date('2022-05-03T11:12:13Z')
      expect(setTimestamp(now)(bom)).toEqual({
        bomFormat: 'CycloneDX',
        specVersion: '1.3',
        version: 1,
        metadata: {
          timestamp: '2022-05-03T11:12:13.000Z',
        },
      })
    })
  })

  describe('addTool', () => {
    it('handles existing tool', () => {
      const bom: BomFile = {
        bomFormat: <const>'CycloneDX',
        specVersion: '1.3',
        version: 1,
        metadata: {
          tools: [
            {
              vendor: 'CycloneDX',
              name: 'Node.js module',
              version: '3.8.0',
            },
          ],
        },
      }
      expect(addTool(bom)).toEqual({
        bomFormat: 'CycloneDX',
        specVersion: '1.3',
        version: 1,
        metadata: {
          tools: [
            {
              vendor: 'CycloneDX',
              name: 'Node.js module',
              version: '3.8.0',
            },
            {
              vendor: 'KaboHub',
              name,
              version,
            },
          ],
        },
      })
    })
    it('handles no existing tool', () => {
      const bom: BomFile = {
        bomFormat: <const>'CycloneDX',
        specVersion: '1.3',
        version: 1,
      }
      expect(addTool(bom)).toEqual({
        bomFormat: 'CycloneDX',
        specVersion: '1.3',
        version: 1,
        metadata: {
          tools: [ {
            vendor: 'KaboHub',
            name,
            version,
          } ],
        },
      })
    })
  })
})
