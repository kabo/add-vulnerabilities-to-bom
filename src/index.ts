import { identity, pipe } from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'
import * as TE from 'fp-ts/lib/TaskEither'
import { generateReqBody, toBomVulns } from './lib/npm'
import { insertVulns, setBomVersionNumber, incBomVersion, setTimestamp, addTool } from './lib/bom'
import { getNpmVulns } from './side-effects/npm'
import type { BomFile, MyError } from './types'

export type { BomFile } from './types'

export const addVulnerabilitiesToBomTE = (bom: BomFile): TE.TaskEither<MyError, BomFile> => pipe(
  bom,
  TE.fromOptionK(() => 'No components')(generateReqBody),
  TE.chain(getNpmVulns),
  TE.map(toBomVulns(bom)),
  TE.map(insertVulns(bom)),
  TE.map(setBomVersionNumber),
  TE.map(incBomVersion),
  TE.map(setTimestamp(new Date())),
  TE.map(addTool)
)

export const addVulnerabilitiesToBom = (bom: BomFile): Promise<BomFile> =>
  addVulnerabilitiesToBomTE(bom)()
    .then(E.matchW(Promise.reject, identity))
