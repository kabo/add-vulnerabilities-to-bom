import { satisfies } from 'compare-versions'
import * as N from 'fp-ts-std/Number'
import { allPass } from 'fp-ts-std/Predicate'
import { values } from 'fp-ts-std/Record'
import { constant, flow, identity, pipe } from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'
import * as O from 'fp-ts/lib/Option'
import * as RONEA from 'fp-ts/lib/ReadonlyNonEmptyArray'
import * as ROA from 'fp-ts/lib/ReadonlyArray'
import * as ROR from 'fp-ts/lib/ReadonlyRecord'
import * as S from 'fp-ts/lib/string'
import { get } from 'spectacles-ts'
import { curry2, guard } from 'fp-ts-std/Function'
import { join } from 'fp-ts-std/Array'
import { stringEq } from './string'
import type {
  BomComponent,
  BomComponents,
  BomAffectsList,
  BomFile,
  BomSeverity,
  BomVulnRatingMethod,
  BomVulnerability,
  BomVulnerabilities,
  Advisory,
  NpmResult,
  NpmRequest
} from '../types'

export const generateReqBody = (bom: BomFile): O.Option<NpmRequest> => pipe(
  bom,
  get('components?'),
  O.map(RONEA.groupBy<BomComponent>(({ group, name }) => group ? `${group}/${name}` : name)),
  O.map(ROR.map(flow(
    ROA.filterMap<BomComponent, string>(get('version?')),
    ROA.uniq(S.Eq)
  )))
)

const satisfiesVersion = curry2(satisfies)

export const getBomRefFor = (name: string, version: string) => (components: BomComponents): BomAffectsList => pipe(
  components,
  ROA.filter(allPass([
    (c) => c.purl?.startsWith(`pkg:npm/${name.replace('@', '%40')}@`) || false,
    (c) => version.split(' ').every(satisfiesVersion(c.version!)),
  ])),
  ROA.map((c) => ({
    ref: c[ 'bom-ref' ]!,
    versions: [ {
      range: `vers:npm/${version.replace(/ /g, '|')}`,
    } ],
  })),
)

export const npmSeverityToBomSeverity =
  guard<string, BomSeverity>([
    [ stringEq('critical'), constant('critical') ],
    [ stringEq('high'), constant('high') ],
    [ stringEq('moderate'), constant('medium') ],
    [ stringEq('low'), constant('low') ],
  ])(constant('unknown'))

export const npmVectorStringToBomMethod = (s: E.Either<string, string>): BomVulnRatingMethod => pipe(
  s,
  E.map(S.split('/')),
  E.map(RONEA.head),
  E.match(
    constant('other'),
    guard<string, BomVulnRatingMethod>([
      [ stringEq('CVSS:3.1'), constant('CVSSv31') ],
      [ stringEq('CVSS:3'), constant('CVSSv3') ],
      [ stringEq('CVSS:2'), constant('CVSSv2') ],
      [ stringEq('OWASP'), constant('OWASP') ],
    ])(constant('other'))
  ),
)

export const npmVectorStringToBomVector = (s: O.Option<string>): string | undefined => pipe(
  s,
  O.map(S.split('/')),
  O.map(RONEA.tail),
  O.map(join('/') as (x: readonly string[]) => string),
  O.match(
    constant(undefined), // eslint-disable-line fp/no-nil
    identity
  )
)

export const toBomVulns = (bom: BomFile) => (result: NpmResult): BomVulnerabilities => pipe(
  result,
  ROR.mapWithIndex((k, v) => ROA.map<Advisory, BomVulnerability>((a) => ({
    cwes: ROA.filterMap(flow(S.replace('CWE-', ''), N.fromString))(a.cwe),
    description: a.title,
    id: a.id.toString(),
    // TODO: bom-ref
    affects: getBomRefFor(k, a.vulnerable_versions)(bom.components!),
    advisories: [ { url: a.url } ],
    ratings: [ {
      severity: npmSeverityToBomSeverity(a.severity),
      score: a.cvss.score,
      vector: npmVectorStringToBomVector(O.fromNullable(a.cvss.vectorString)),
      method: npmVectorStringToBomMethod(E.fromNullable('other')(a.cvss.vectorString)),
    } ],
  }))(v)),
  values,
  ROA.flatten
)
