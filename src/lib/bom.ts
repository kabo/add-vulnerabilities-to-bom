import { when } from 'fp-ts-std/Function'
import { constant, pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import { get, modify, set } from 'spectacles-ts'
import { name, version } from '../../package.json'
import type {
  BomFile,
  BomVulnerabilities,
  BomMetaTool,
} from '../types'

export const insertVulns = (bom: BomFile) => (vulnerabilities: BomVulnerabilities): BomFile => ({
  ...bom,
  vulnerabilities,
})

export const setBomVersionNumber = (bom: BomFile): BomFile => ({
  ...bom,
  specVersion: '1.4',
})

export const incBomVersion = (bom: BomFile): BomFile => ({
  ...bom,
  version: bom.version + 1,
})

const isNil = (x: unknown) => x === null || x === undefined
const ensureMetadata = (bom: BomFile): BomFile => pipe(
  bom,
  modify('metadata', when(isNil)(constant({})))
)

export const setTimestamp = (now: Date) => (bom: BomFile): BomFile => pipe(
  bom,
  ensureMetadata,
  set('metadata?.timestamp', now.toISOString())
)

const tool: BomMetaTool = {
  vendor: 'KaboHub',
  name,
  version,
}
export const addTool = (bom: BomFile): BomFile => pipe(
  bom,
  ensureMetadata,
  set('metadata?.tools', pipe(
    bom,
    get('metadata?.tools?'),
    O.match(
      constant([ tool ]),
      (tools) => ([ ...tools, tool ])
    )
  ))
)
