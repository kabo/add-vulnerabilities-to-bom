import * as S from 'fp-ts/lib/string'
import { curry2 } from 'fp-ts-std/Function'

export const stringEq = curry2(S.Eq.equals)
