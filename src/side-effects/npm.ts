/* global globalThis */
import fetch from 'cross-fetch'
import * as TE from 'fp-ts/lib/TaskEither'
import { fetchJSON } from 'fp-fetch'
import type { MyError, NpmResult, NpmRequest } from '../types'

// eslint-disable-next-line fp/no-mutation
globalThis.fetch = fetch
const registryUrl = 'https://registry.npmjs.org/-/npm/v1/security/advisories/bulk'

export const getNpmVulns = (req: NpmRequest): TE.TaskEither<MyError, NpmResult> =>
  fetchJSON(registryUrl, {
    method: 'POST',
    body: JSON.stringify(req),
    headers: {
      'Content-Type': 'application/json',
    },
  })
