import { FetchError } from 'fp-fetch'

export interface BomComponent {
  readonly type: 'application' | 'framework' | 'library' | 'container' | 'operating-system' | 'device' | 'firmware' | 'file'
  readonly name: string
  readonly 'bom-ref'?: string
  readonly author?: string
  readonly group?: string
  readonly version?: string
  readonly description?: string
  readonly purl?: string
}
export type BomComponents = readonly BomComponent[]
export interface BomAffectsVersion {
  readonly version?: string
  readonly range?: string
  readonly status?: 'affected' | 'unaffected' | 'unknown'
}
export interface BomAffects {
  readonly ref: string
  readonly versions?: readonly BomAffectsVersion[]
}
export type BomAffectsList = readonly BomAffects[]
export interface BomDependency {
  readonly ref: string
  readonly dependsOn?: readonly string[]
}
export type BomDependencies = readonly BomDependency[]
export type BomSeverity = 'critical' | 'high' | 'medium' | 'low' | 'info' | 'none' | 'unknown'
export type BomVulnRatingMethod = 'CVSSv2' | 'CVSSv3' | 'CVSSv31' | 'OWASP' | 'other'
export interface BomVulnerability {
  readonly 'bom-ref'?: string
  readonly id?: string
  readonly description?: string
  readonly cwes?: readonly number[]
  readonly affects?: BomAffectsList
  readonly advisories?: readonly {
    readonly title?: string
    readonly url: string
  }[]
  readonly ratings?: readonly {
    readonly score?: number
    readonly severity?: BomSeverity
    readonly method?: BomVulnRatingMethod
    readonly vector?: string
  }[]
}
export type BomVulnerabilities = readonly BomVulnerability[]
export interface BomMetaTool {
  readonly vendor?: string
  readonly name?: string
  readonly version?: string
}
export type BomMetaTools = readonly BomMetaTool[]
export interface BomMetadata {
  readonly timestamp?: string
  readonly tools?: BomMetaTools
}
export interface BomFile {
  readonly bomFormat: 'CycloneDX'
  readonly specVersion: string
  readonly version: number
  readonly serialNumber?: string
  readonly components?: BomComponents
  readonly vulnerabilities?: BomVulnerabilities
  readonly dependencies?: BomDependencies
  readonly metadata?: BomMetadata
}
export type NpmRequest = Record<string, readonly string[]>
export interface Advisory {
  readonly id: number
  readonly url: string
  readonly title: string
  readonly severity: string
  readonly vulnerable_versions: string
  readonly cwe: readonly string[]
  readonly cvss: {
    readonly score: number
    readonly vectorString: string
  }
}
export type Advisories = readonly Advisory[]
export type NpmResult = Record<string, Advisories>
export type MyError = string | FetchError<unknown>
