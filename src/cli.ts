import { readFileSync } from 'fs'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { addVulnerabilitiesToBom } from './index'
import { BomFile } from './types'

const argv = yargs(hideBin(process.argv))
  .option('f', {
    alias: 'file',
    type: 'string',
    describe: 'path to bom.json',
    default: 'bom.json',
  })
  .help()
  .parseSync()

const bom: BomFile = JSON.parse(readFileSync(argv.file as string, { encoding: 'utf-8' }))

// eslint-disable-next-line fp/no-unused-expression
addVulnerabilitiesToBom(bom)
  .then(JSON.stringify)
  .then(console.log)
  .catch((err) => { // eslint-disable-line better/explicit-return, fp/no-nil
    console.error(err) // eslint-disable-line fp/no-unused-expression
    process.exitCode = 1 // eslint-disable-line fp/no-mutation
  })
