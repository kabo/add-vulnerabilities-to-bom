# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/kabo/add-vulnerabilities-to-bom/compare/v0.0.2...v0.0.3) (2022-05-07)


### Bug Fixes

* handle npm version ranges ([57cd813](https://gitlab.com/kabo/add-vulnerabilities-to-bom/commit/57cd813ea22de7aca7d55d791dcc04862a604818))

### [0.0.2](https://gitlab.com/kabo/add-vulnerabilities-to-bom/compare/v0.0.1...v0.0.2) (2022-05-07)


### Features

* added versions to vulnerabilities ([b9ac484](https://gitlab.com/kabo/add-vulnerabilities-to-bom/commit/b9ac48403eab91f9c809729e738192d901e0310f))

### 0.0.1 (2022-05-03)
